import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit, OnDestroy {
  user: {id: number, name: string};
  paramsSubscription: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    // if we are already on the component the url is redirecting us to, ngOnInit will not rerun, therefore we will not get updated parameters
    this.user = {
      id: this.route.snapshot.params['id'],
      name: this.route.snapshot.params['name'],
    }

    // observables - a featuer by 3rd party package which allows us to work with async tasks; an easy way to subscribe to some event that might happen in the future
    // when the component is destroyed, the subscription gets cleaned automatically in this case
    this.paramsSubscription = this.route.params.subscribe((params: Params) => {
      this.user.id = params['id'];
      this.user.name = params['name'];
    })

    // if you know that the component will always be recreated when it's reached, it's safe to use snapshot
  }

  ngOnDestroy() {
    // a must-do with your own observables, but not needed with defaults
    this.paramsSubscription.unsubscribe();
  }

}
